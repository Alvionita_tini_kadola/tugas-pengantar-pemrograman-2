import java.util.*;

 class calculator {
	private double value_1;
	private double value_2;
	private double result;

	public void setValue_1(double val){
	value_1 = val;
	}

	public double getValue_1(){
		return value_1;
	}

	public void setValue_2(double val){
	value_2 = val;
	}

	public double getValue_2(){
		return value_2;
	}

	public double getresult(){
		return result;
	}
	
	public void addition(){
		result = value_1 + value_2;
	}

	public void reduction(){
		result = value_1 - value_2;
	}
	
	public void product(){
		result = value_1*value_2;
	}
	
	public void divide(){
		result = value_1/value_2;
	}
	
	public void root(){
		result = Math.sqrt(value_1);
	}
	
	public void power(){
		result = Math.pow(value_1, value_2);
	}

	}

	public class simplecalculator{
		
		public static void main (String [] args){
			calculator object = new calculator();
			Scanner input = new Scanner(System.in);
			boolean kontinu = false;
			int choice;
			
			do{
				try{
				System.out.println("Kalkulator");
				System.out.println("1. Penjumlahan");
				System.out.println("2. Pengurangan");
				System.out.println("3. Perkalian");
				System.out.println("4. Pembagian");
				System.out.println("5. Akar");
				System.out.println("6. Pangkat");
				System.out.println("7. Exit");
				
				System.out.println("");
				System.out.print("Masukan Pilihan Anda : ");
				choice = input.nextInt();
				System.out.println("");
				
				
				if(choice==1){
					try{
					System.out.print("Masukan Angka Pertama : ");
					double val_1=input.nextDouble();
					object.setValue_1(val_1);
					System.out.println();
					
					System.out.print("Masukan Angka Kedua : ");
					double val_2=input.nextDouble();
					object.setValue_2(val_2);
					System.out.println();
					
					object.addition();
					System.out.println(val_1+" + "+val_2+" = "+object.getresult());
					System.out.println();
					}
					
					catch(InputMismatchException e){
					System.out.println("Inputan Anda Tidak sesuai");
					System.out.println();
					input.nextLine();
					continue;	
					}
						
					catch (NoSuchElementException e){
					System.out.println("Don't Press Ctrl+Z");
					}
					
				}
				
				else if (choice==2){
					try{
					System.out.print("Masukan Angka Pertama : ");
					double val_1=input.nextDouble();
					object.setValue_1(val_1);
					System.out.println();
						
					System.out.print("Masukan Angka Kedua : ");
					double val_2=input.nextDouble();
					object.setValue_2(val_2);
					System.out.println();
					
						
					
					object.reduction();
					System.out.println(val_1+" - "+val_2+" = "+object.getresult());	
					System.out.println();
					}
					
					catch(InputMismatchException e){
					System.out.println("Inputan Anda Tidak sesuai");
					System.out.println();
					input.nextLine();
					continue;	
					}
						
					catch (NoSuchElementException e){
					System.out.println("Don't Press Ctrl+Z");
					}
					
				}
				
				else if(choice==3){
					try{
						System.out.print("Masukan Angka Pertama : ");
						double val_1=input.nextDouble();
						object.setValue_1(val_1);
						System.out.println();
						
						System.out.print("Masukan Angka Kedua : ");
						double val_2=input.nextDouble();
						object.setValue_2(val_2);
						System.out.println();
						
						object.product();
						System.out.println(val_1+" x "+val_2+" = "+object.getresult());	
						System.out.println();
						
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Tidak sesuai");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
						
					}
				
				else if(choice==4){
					try{
						System.out.print("Masukan Angka Pertama : ");
						double val_1=input.nextDouble();
						object.setValue_1(val_1);
						System.out.println();
						
						System.out.print("Masukan Angka Kedua : ");
						double val_2=input.nextDouble();
						object.setValue_2(val_2);
						System.out.println();
						
						object.divide();
						System.out.println(val_1+" : "+val_2+" = "+object.getresult());	
						System.out.println();
						
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Tidak sesuai");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
					}
				
				else if(choice==5){
					try{
						System.out.println("Masukan Angka yang akan di akarkan : ");
						double val_1=input.nextDouble();
						object.setValue_1(val_1);
						System.out.println();
						
						object.root();
						System.out.println("Akar dari "+val_1+" = "+object.getresult());	
						System.out.println();
					
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
					}
				
				else if(choice==6){
					try{
						System.out.print("Masukan Bilangan : ");
						double val_1=input.nextDouble();
						object.setValue_1(val_1);
						System.out.println();
						
						System.out.print("Masukan Pangkat : ");
						double val_2=input.nextDouble();
						object.setValue_2(val_2);
						System.out.println();
						
						object.power();
						System.out.println(val_1+" Pangkat "+val_2+" = "+object.getresult());	
						System.out.println();
					
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Tidak sesuai");
						System.out.println();
						input.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Don't Press Ctrl+Z");
						}
						
					}
				
				else if(choice==7){
					System.out.println("Terima Kasih...");
					break;
					
				}
				
				else{
					System.out.println("Inputan Anda Tidak sesuai");
					System.out.println();
				}
				}
				
				catch(InputMismatchException e){
				System.out.println("Inputan Anda Tidak sesuai");
				System.out.println();
				input.nextLine();
				continue;	
				}
				
				catch (NoSuchElementException e){
					System.out.println("Don't Press Ctrl+Z");
				}

				}while(kontinu=true);

		}

	}